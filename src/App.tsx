import { useEffect, useState } from "react";
import "./App.css";
import AddTask from "./components/AddTask";
import TaskLists from "./components/TaskLists";

let taskListOnLocalStorage = localStorage.getItem("taskList");
const todoLists = taskListOnLocalStorage
  ? JSON.parse(taskListOnLocalStorage)
  : [];

function App() {
  const [taskList, setTaskList] = useState<any>(todoLists);

  function addNewTaskLists(newTask: any) {
    taskList
      ? setTaskList((prevState: any) => [...prevState, newTask])
      : setTaskList([newTask]);
    // setTaskList(taskList.push(newTask));
  }
  useEffect(() => {
    localStorage.setItem("taskList", JSON.stringify(taskList));
  }, [taskList]);
  function updateTodoContent(updatedTask: any) {
    console.log(updatedTask);
  }

  return (
    <div className="todo-container">
      <AddTask updateTaskLists={addNewTaskLists} />
      <TaskLists
        taskLists={taskList}
        setTaskList={setTaskList}
        updateTodo={updateTodoContent}
      />
    </div>
  );
}

export default App;
