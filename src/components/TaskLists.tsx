import TodoContent from "./TodoContent";

const TaskLists = (props: any) => {
  const { taskLists, setTaskList } = props;
  return (
    <div className="todo--task-lists">
      {taskLists.map((todo: any, index: any) => (
        <TodoContent
          todo={todo}
          key={todo.id}
          index={index}
          taskLists={taskLists}
          setTaskList={setTaskList}
        />
      ))}
    </div>
  );
};

export default TaskLists;
