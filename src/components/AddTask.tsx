import { useState } from "react";

const AddTask = (props: any) => {
  const updateTodoLists = props.updateTaskLists;
  const [newTask, setNewTask] = useState("");

  function createNewTask(task: string) {
    return {
      id: Date.now(),
      todoTask: task,
      isCompleted: false,
    };
  }

  function setTaskOnLocalStorage(task: string) {
    if (task) {
      const newTask = createNewTask(task);
      updateTodoLists(newTask);
      setNewTask("");
    } else {
      alert("Invalid Task");
    }
  }
  return (
    <div>
      <h1 className="main-title">Add todo Lists</h1>
      <input
        type="text"
        className="input-todo"
        placeholder="Enter your todo task"
        value={newTask}
        onChange={(e) => {
          setNewTask(e.target.value);
        }}
        onKeyDown={(e) => {
          if (e.key == "Enter") setTaskOnLocalStorage(newTask);
        }}
      />
      <input
        type="submit"
        className="btn-add-todo"
        value="Add"
        onClick={() => setTaskOnLocalStorage(newTask)}
      />
    </div>
  );
};

export default AddTask;
