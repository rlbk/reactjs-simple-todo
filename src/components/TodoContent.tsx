import { useState } from "react";
import "./TodoContent.css";

const TodoContent = ({ todo, index, taskLists, setTaskList }: any) => {
  const [isEdit, setIsEdit] = useState(false);
  const [editValue, setEditValue] = useState(todo.todoTask);

  function getCurrentEditTask() {
    setIsEdit(true);
  }

  function editCurrentTask(event: any) {
    setEditValue(event.target.value);
  }

  function editingCompleteUsingKey(event: any, id: any) {
    if (event.key == "Enter") {
      setIsEdit(false);
      updateEditValueOnStorage(id);
    }
  }

  function checkBoxHandler(event: any, id: any) {
    const currentTask = taskLists.filter((todo: any) => todo.id == id)[0];
    currentTask.isCompleted = event.target.checked ? true : false;
    taskLists.splice(index, 1, currentTask);
    setTaskList([...taskLists]);
    localStorage.setItem("taskList", JSON.stringify(taskLists));
  }

  function updateEditValueOnStorage(id: any) {
    const currentTask = taskLists.filter((todo: any) => todo.id == id)[0];
    currentTask.todoTask = editValue;
    taskLists.splice(index, 1, currentTask);
    updateOnLocalStorage(taskLists);
  }

  function removeValueFromStorage() {
    taskLists.splice(index, 1);
    updateOnLocalStorage(taskLists);
  }

  function updateOnLocalStorage(newTaskList: any) {
    setTaskList([...newTaskList]);
    localStorage.setItem("taskList", JSON.stringify(taskLists));
  }

  return (
    <div className="todo--task-item">
      {isEdit ? (
        <input
          type="text"
          id="edit-todo-task"
          value={editValue}
          onChange={(e) => {
            editCurrentTask(e);
          }}
          onKeyDown={(e) => editingCompleteUsingKey(e, todo.id)}
          autoFocus
        />
      ) : (
        <span>
          <input
            type="checkbox"
            onChange={(e) => checkBoxHandler(e, todo.id)}
            title="complete task"
            checked={todo.isCompleted}
          />
          <span
            style={{ marginLeft: "0.3rem" }}
            className={todo.isCompleted ? "completedTask" : ""}
          >
            {editValue}
          </span>

          <span
            style={{ cursor: "pointer", marginLeft: "0.5rem" }}
            onClick={() => getCurrentEditTask()}
          >
            📝
          </span>
        </span>
      )}
      <span
        style={{ cursor: "pointer", marginLeft: "0.5rem" }}
        onClick={removeValueFromStorage}
      >
        ❌
      </span>
    </div>
  );
};

export default TodoContent;
